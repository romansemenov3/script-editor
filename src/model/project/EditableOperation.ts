import {AbstractOperation} from "@/model/operation/Operation";
import {Point} from "@/model/ui/TransformMatrix";
import {CallableOperationDefinition, OperationDefinition} from "@/model/definition/Definition";
import {OperationElement} from "@/model/ui/element/OperationElement";
import {CallablePass} from "@/model/operation/library/pass/CallablePass";
import {ValuePass} from "@/model/operation/library/pass/ValuePass";

export class EditableOperation {
    private readonly _operation: AbstractOperation;
    private readonly _element: OperationElement;

    private _position: Point;
    private _errors: Error[] = [];
    private _selected: boolean = false;

    constructor(operation: AbstractOperation, position: Point = {x: 0, y : 0}) {
        this._operation = operation;
        this._position = position;
        if(operation instanceof CallablePass || operation instanceof ValuePass) {
            this._element = OperationElement.fromPass(operation);
        } else {
            this._element = OperationElement.fromAnyOperation(operation);
        }
    }

    get selected(): boolean {
        return this._selected;
    }

    set selected(value: boolean) {
        this._selected = value;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }

    get definition() : OperationDefinition {
        return this.operation.definition;
    }

    get position(): Point {
        return this._position;
    }

    set position(value: Point) {
        this._position = value;
    }

    get errors(): Error[] {
        return this._errors;
    }

    get isCallable() : boolean {
        return this.operation.definition instanceof CallableOperationDefinition;
    }

    get element(): OperationElement {
        return this._element;
    }
}
