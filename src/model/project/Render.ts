import {ConstantGetter} from "@/model/operation/library/getter/ConstantGetter";
import {AbstractCallerOperation, AbstractOperation, ProvidedInput, ProvidedOutcome} from "@/model/operation/Operation";
import {Validation} from "@/model/validation/Validation";
import {EditableOperation} from "@/model/project/EditableOperation";
import {CallerOperationDefinition, InputDefinition} from "@/model/definition/Definition";
import {ValidationErrors} from "@/model/validation/ValidationError";
import {LabeledSocketElement} from "@/model/ui/element/LabeledSocketElement";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {LabeledInputTextSocketElement} from "@/model/ui/element/LabeledInputTextSocketElement";
import {LabeledCheckboxSocketElement} from "@/model/ui/element/LabeledCheckboxSocketElement";

interface ClonedOperation {
    readonly source: AbstractOperation;
    readonly clone: AbstractOperation;
}

export class Render {

    private readonly _start: AbstractCallerOperation;
    private readonly _operations: AbstractOperation[] = [];
    private readonly _constants: ConstantGetter[] = [];
    private readonly _errors: ValidationErrors;

    constructor(start: AbstractCallerOperation, operations: EditableOperation[]) {
        let clonedCallers : ClonedOperation[] = [];
        let clonedOperations : ClonedOperation[] = [];
        let clonedStart: AbstractCallerOperation | null = null;
        for(let operation of operations) {
            if(operation.definition instanceof CallerOperationDefinition) {
                let clonedOperation = operation.operation.clone();
                this._operations.push(clonedOperation);

                let clone : ClonedOperation = {source: operation.operation, clone: clonedOperation};
                clonedCallers.push(clone);
                clonedOperations.push(clone);

                Render.generateConstantGetters(operation, clonedOperation)
                    .forEach(constant => this._constants.push(constant));

                if(operation.operation === start) {
                    clonedStart = clonedOperation as AbstractCallerOperation;
                }
            } else {
                let clonedOperation = operation.operation.clone();
                this._operations.push(clonedOperation);

                let clone : ClonedOperation = {source: operation.operation, clone: clonedOperation};
                clonedOperations.push(clone);

                Render.generateConstantGetters(operation, clonedOperation)
                    .forEach(constant => this._constants.push(constant));
            }
        }
        Render.replaceInputsWithClones(clonedOperations);
        Render.replaceOutcomesWithClones(clonedCallers);
        if(clonedStart) {
            this._start = clonedStart;
        } else {
            throw new Error("Could not clone start");
        }
        this._errors = Validation.validate(this._start, this._operations, this._constants);
    }

    get errors(): ValidationErrors {
        return this._errors;
    }

    render(indent: number) : string {
        if(this._errors.hasErrors) {
            throw new Error("Can not render with errors");
        }
        return (this._start).renderCall(indent);
    }

    private static generateConstantGetters(source: EditableOperation, clone: AbstractOperation) : ConstantGetter[] {
        let result: ConstantGetter[] = [];
        for(let inputName in source.definition.inputs) {
            if(source.operation.providedInputs[inputName] === undefined) {
                let constantGetter = Render.generateConstantGetter(
                    source.definition.inputs[inputName],
                    inputName,
                    source.element.body.inputs
                );
                if (constantGetter) {
                    result.push(constantGetter);
                    clone.providedInputs[inputName] = new ProvidedInput(constantGetter, "Value");
                }
            }
        }
        return result;
    }

    private static generateConstantGetter(definition: InputDefinition, inputName: string, inputs: Record<string, LabeledSocketElement>) : ConstantGetter | null {
        if (definition.collection === DataCollection.SINGLE) {
            switch (definition.type) {
                case DataType.STRING:
                    return new ConstantGetter(
                        (inputs[inputName] as LabeledInputTextSocketElement).input.text,
                        DataType.STRING
                    );
                case DataType.NUMBER:
                    return new ConstantGetter(
                        (inputs[inputName] as LabeledInputTextSocketElement).input.text || "0",
                        DataType.NUMBER
                    );
                case DataType.BOOLEAN:
                    return new ConstantGetter(
                        (inputs[inputName] as LabeledCheckboxSocketElement).checked ? "(1 -eq 1)" : "(1 -eq 0)",
                        DataType.BOOLEAN
                    );
            }
        }
        return null;
    }

    private static replaceOutcomesWithClones(clones: ClonedOperation[]) {
        for(let clone of clones) {
            if(clone.clone instanceof AbstractCallerOperation) {
                for(let outcomeName in clone.clone.providedOutcomes) {
                    let providedOutcome = clone.clone.providedOutcomes[outcomeName];
                    for(let source of clones) {
                        if(source.source instanceof AbstractCallerOperation && providedOutcome.target === source.source) {
                            clone.clone.providedOutcomes[outcomeName] = new ProvidedOutcome(source.clone as AbstractCallerOperation);
                        }
                    }
                }
            }
        }
    }

    private static replaceInputsWithClones(clones: ClonedOperation[]) {
        for(let clone of clones) {
            for(let inputName in clone.clone.providedInputs) {
                let providedInput = clone.clone.providedInputs[inputName];
                for(let source of clones) {
                    if(providedInput.source === source.source) {
                        clone.clone.providedInputs[inputName] = new ProvidedInput(source.clone, providedInput.outputName);
                    }
                }
            }
        }
    }

}
