import {EditableOperation} from "@/model/project/EditableOperation";
import {Point} from "@/model/ui/TransformMatrix";
import {ColorUtils} from "@/service/ColorUtils";

export class AbstractLink {

    private readonly _fromOperation: EditableOperation;
    private readonly _toOperation: EditableOperation;
    private _selected: boolean = false;

    constructor(fromOperation: EditableOperation, toOperation: EditableOperation) {
        this._fromOperation = fromOperation;
        this._toOperation = toOperation;
    }

    get selected(): boolean {
        return this._selected;
    }

    set selected(value: boolean) {
        this._selected = value;
    }

    get fromOperation(): EditableOperation {
        return this._fromOperation;
    }

    get toOperation(): EditableOperation {
        return this._toOperation;
    }
}

export class ValueLink extends AbstractLink {

    private readonly _fromOutput: string;
    private readonly _toInput: string;

    constructor(fromOperation: EditableOperation, fromOutput: string, toOperation: EditableOperation, toInput: string) {
        super(fromOperation, toOperation);
        this._fromOutput = fromOutput;
        this._toInput = toInput;
    }

    get fromOutput(): string {
        return this._fromOutput;
    }

    get toInput(): string {
        return this._toInput;
    }

    get fromOutputPosition() : Point {
        return {
            x: this.fromOperation.position.x + this.fromOperation.element.width,
            y: this.fromOperation.position.y + this.fromOperation.element.body.outputsTop[this.fromOutput]
        }
    }

    get toInputPosition() : Point {
        return {
            x: this.toOperation.position.x,
            y: this.toOperation.position.y + this.toOperation.element.body.inputsTop[this.toInput]
        }
    }

    get color() : string {
        return ColorUtils.getTypeColor(this.fromOperation.definition.outputs[this.fromOutput].type);
    }

    get path() : string {
        let from : Point = this.fromOutputPosition;
        let to : Point = this.toInputPosition;
        let delta : Point = { x: to.x - from.x, y: to.y - from.y };
        let distance = Math.sqrt(delta.x*delta.x + delta.y*delta.y);
        let curvature = distance/2;

        return "M" + from.x + " " + from.y + " C " + (from.x + curvature) + " " + from.y + ", " +
            (to.x - curvature) + " " + to.y + ", " + to.x + " " + to.y;
    }
}

export class CallLink extends AbstractLink {

    private readonly _fromOutcome: string;

    constructor(fromOperation: EditableOperation, fromOutcome: string, toOperation: EditableOperation) {
        super(fromOperation, toOperation);
        this._fromOutcome = fromOutcome;
    }

    get fromOutcome(): string {
        return this._fromOutcome;
    }

    get fromOutcomePosition() : Point {
        return {
            x: this.fromOperation.position.x + this.fromOperation.element.width,
            y: this.fromOperation.position.y + this.fromOperation.element.body.outcomesTop[this.fromOutcome]
        }
    }

    get toCallPosition() : Point {
        return {
            x: this.toOperation.position.x,
            y: this.toOperation.position.y
        }
    }

    get path() : string {
        let from : Point = this.fromOutcomePosition;
        let to : Point = this.toCallPosition;
        let delta : Point = { x: to.x - from.x, y: to.y - from.y };
        let distance = Math.sqrt(delta.x*delta.x + delta.y*delta.y);
        let curvature = distance/2;

        return "M" + from.x + " " + from.y + " C " + (from.x + curvature) + " " + from.y + ", " +
            (to.x - curvature) + " " + to.y + ", " + to.x + " " + to.y;
    }

}
