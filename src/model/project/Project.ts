import {
    AbstractCallerOperation,
    AbstractOperation,
    AbstractStartOperation,
    ProvidedInput,
    ProvidedOutcome
} from "@/model/operation/Operation";
import {EditableOperation} from "@/model/project/EditableOperation";
import {CallLink, ValueLink} from "@/model/project/Link";
import {Point} from "@/model/ui/TransformMatrix";
import {AbstractStartDefinition} from "@/model/definition/AbstractStartDefinition";
import {CallablePass} from "@/model/operation/library/pass/CallablePass";
import {ValuePass} from "@/model/operation/library/pass/ValuePass";
import {Variable} from "@/model/operation/library/entity/Variable";
import {Render} from "@/model/project/Render";

export class Project {

    private readonly _start: EditableOperation;
    private readonly _operations: EditableOperation[];

    private readonly _valueLinks: ValueLink[] = [];
    private readonly _callLinks: CallLink[] = [];

    private readonly _variables: Variable[] = [];

    constructor(start: AbstractStartOperation) {
        this._start = new EditableOperation(start, {x: 100, y: 100});
        this._operations = [this._start];
    }

    get start(): EditableOperation {
        return this._start;
    }

    get operations(): EditableOperation[] {
        return this._operations;
    }

    get commonOperations(): EditableOperation[] {
        let result: EditableOperation[] = [];
        this._operations.forEach(operation => {
            if(!(operation.operation instanceof CallablePass || operation.operation instanceof ValuePass)) {
                result.push(operation);
            }
        });
        return result;
    }

    get passOperations(): EditableOperation[] {
        let result: EditableOperation[] = [];
        this._operations.forEach(operation => {
            if(operation.operation instanceof CallablePass || operation.operation instanceof ValuePass) {
                result.push(operation);
            }
        });
        return result;
    }

    get valueLinks(): ValueLink[] {
        return this._valueLinks;
    }

    get callLinks(): CallLink[] {
        return this._callLinks;
    }

    get selectedCallLinks() : CallLink[] {
        let selectedCallLinks: CallLink[] = [];
        this.callLinks.forEach(callLink => {
            if(callLink.selected) {
                selectedCallLinks.push(callLink)
            }
        });
        return selectedCallLinks;
    }

    get selectedValueLinks() : ValueLink[] {
        let selectedValueLinks: ValueLink[] = [];
        this.valueLinks.forEach(valueLink => {
            if(valueLink.selected) {
                selectedValueLinks.push(valueLink);
            }
        });
        return selectedValueLinks;
    }

    get selectedOperations() : EditableOperation[] {
        let selectedOperations: EditableOperation[] = [];
        this.operations.forEach(operation => {
            if(operation.selected) {
                selectedOperations.push(operation);
            }
        });
        return selectedOperations;
    }

    addOperation(operation: AbstractOperation, position: Point) : EditableOperation {
        let result = new EditableOperation(operation, position);
        this._operations.push(result);
        return result;
    }

    removeOperation(operation: EditableOperation) {
        let callLinksToRemove: CallLink[] = [];
        for(let callLink of this._callLinks) {
            if(callLink.fromOperation === operation || callLink.toOperation === operation) {
                callLinksToRemove.push(callLink);
            }
        }
        callLinksToRemove.forEach(callLink => this.removeCallLink(callLink));

        let valueLinksToRemove: ValueLink[] = [];
        for(let valueLink of this._valueLinks) {
            if(valueLink.fromOperation === operation || valueLink.toOperation === operation) {
                valueLinksToRemove.push(valueLink);
            }
        }
        valueLinksToRemove.forEach(valueLink => this.removeValueLink(valueLink));

        let index = this._operations.indexOf(operation);
        if(index >= 0) {
            this._operations.splice(index, 1);
        }
    }

    addValueLink(valueLink: ValueLink) {
        let existingLink = this.findValueLinkWithSameInput(valueLink);
        if(existingLink) {
            this.removeValueLink(existingLink);
        }

        this._valueLinks.push(valueLink);
        valueLink.toOperation.operation.providedInputs[valueLink.toInput] =
            new ProvidedInput(valueLink.fromOperation.operation, valueLink.fromOutput);
    }

    removeValueLink(valueLink: ValueLink) {
        let index = this._valueLinks.indexOf(valueLink);
        if(index >= 0) {
            this._valueLinks.splice(index, 1);
        }
        delete valueLink.toOperation.operation.providedInputs[valueLink.toInput];
    }

    findValueLinkWithSameInput(valueLink: ValueLink) : ValueLink | null {
        for(let existingValueLink of this._valueLinks) {
            if(existingValueLink.toInput === valueLink.toInput && existingValueLink.toOperation === valueLink.toOperation) {
                return existingValueLink;
            }
        }
        return null;
    }

    addCallLink(callLink: CallLink) {
        let existingLink = this.findCallLinkWithSameOutcome(callLink);
        if(existingLink) {
            this.removeCallLink(existingLink);
        }

        this._callLinks.push(callLink);
        (callLink.fromOperation.operation as AbstractCallerOperation).providedOutcomes[callLink.fromOutcome] =
            new ProvidedOutcome(callLink.toOperation.operation as AbstractCallerOperation);
    }

    removeCallLink(callLink: CallLink) {
        let index = this._callLinks.indexOf(callLink);
        if(index >= 0) {
            this._callLinks.splice(index, 1);
        }
        delete (callLink.fromOperation.operation as AbstractCallerOperation).providedOutcomes[callLink.fromOutcome];
    }

    findCallLinkWithSameOutcome(callLink: CallLink) : CallLink | null {
        for(let existingCallLink of this._callLinks) {
            if(existingCallLink.fromOutcome === callLink.fromOutcome && existingCallLink.fromOperation === callLink.fromOperation) {
                return existingCallLink;
            }
        }
        return null;
    }

    render(indent: number) : string {
        let render = new Render(this.start.operation as AbstractStartOperation, this.operations);
        if(render.errors.hasErrors) {
            console.log(render.errors.invalidOperations);
            return "Has errors";
        }
        return render.render(indent);
    }

    deleteSelected() {
        this.selectedCallLinks.forEach(callLink => this.removeCallLink(callLink));
        this.selectedValueLinks.forEach(valueLink => this.removeValueLink(valueLink));
        this.selectedOperations.forEach(operation => {
            if(!(operation.definition instanceof AbstractStartDefinition)) {
                this.removeOperation(operation);
            }
        });
    }
}
