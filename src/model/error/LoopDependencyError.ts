import {AbstractOperation} from "@/model/operation/Operation";

export class LoopDependencyError extends Error {

    private readonly _operation: AbstractOperation;

    constructor(operation: AbstractOperation) {
        super("Loop dependency detected");
        this._operation = operation;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }
}
