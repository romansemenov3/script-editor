export class CallableOperationRequiredError extends Error {

    constructor(message?: string) {
        super(message || "Callable operation is required");
    }
}
