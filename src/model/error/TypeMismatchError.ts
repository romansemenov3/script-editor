import {AbstractOperation, ProvidedInput} from "@/model/operation/Operation";
import {OutputDefinition} from "@/model/definition/Definition";

export class TypeMismatchError extends Error {
    constructor(target: AbstractOperation, input: string, output: OutputDefinition) {
        super("Operation " + target.definition.name + " input " + input + " is not " + output.type + " " + output.collection);
    }
}
