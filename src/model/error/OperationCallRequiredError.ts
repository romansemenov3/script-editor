import {AbstractOperation} from "@/model/operation/Operation";

export class OperationCallRequiredError extends Error {

    private readonly _operation: AbstractOperation;

    constructor(operation: AbstractOperation) {
        super("Operation call is required");
        this._operation = operation;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }
}
