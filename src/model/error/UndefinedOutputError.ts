import {AbstractOperation} from "@/model/operation/Operation";


export class UndefinedOutputError extends Error {

    constructor(target: AbstractOperation, output: string) {
        super("Operation " + target.definition.name + " has no " + output + " output");
    }

}
