import {AbstractOperation} from "@/model/operation/Operation";


export class UndefinedInputError extends Error {

    constructor(target: AbstractOperation, input: string) {
        super("Operation " + target.definition.name + " has no " + input + " input");
    }

}
