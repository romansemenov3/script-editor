import {AbstractOperation} from "@/model/operation/Operation";

export class EmptyInputError extends Error {

    private readonly _operation: AbstractOperation;

    constructor(operation: AbstractOperation) {
        super("Empty input detected");
        this._operation = operation;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }
}
