import {AbstractCallerOperation} from "@/model/operation/Operation";


export class UndefinedOutcomeError extends Error {

    constructor(target: AbstractCallerOperation, outcome: string) {
        super("Operation " + target.definition.name + " has no " + outcome + " outcome");
    }

}
