import {AbstractOperation} from "@/model/operation/Operation";

export class OutOfScopeAccessError extends Error {

    private readonly _operation: AbstractOperation;

    constructor(operation: AbstractOperation) {
        super("Attempted to access operation out of scope");
        this._operation = operation;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }
}
