import {CallerOperationDefinition, OperationDefinition, OutputDefinition} from "@/model/definition/Definition";
import {AbstractStartDefinition} from "@/model/definition/AbstractStartDefinition";

export class ProvidedInput {
    private readonly _source: AbstractOperation;
    private readonly _outputName: string;

    constructor(source: AbstractOperation, outputName: string) {
        this._source = source;
        this._outputName = outputName;
    }

    get source(): AbstractOperation {
        return this._source;
    }

    get outputName(): string {
        return this._outputName;
    }

    get output(): OutputDefinition {
        return this._source.definition.outputs[this._outputName];
    }

    get rendered() : string {
        return this._source.renderOutput(this._outputName);
    }
}

export class ProvidedOutcome {
    private readonly _target: AbstractCallerOperation;

    constructor(target: AbstractCallerOperation) {
        this._target = target;
    }

    get target(): AbstractCallerOperation {
        return this._target;
    }

    render(indent: number) {
        return this._target.renderCall(indent);
    }
}

export abstract class AbstractOperation {
    private readonly _definition: OperationDefinition;
    private _providedInputs: Record<string, ProvidedInput> = {};

    constructor(definition: OperationDefinition)
    constructor(source: AbstractOperation)
    constructor(arg: OperationDefinition | AbstractOperation)
    constructor(arg: OperationDefinition | AbstractOperation) {
        if(arg instanceof OperationDefinition) {
            this._definition = arg;
        } else {
            this._definition = arg.definition;
            for(let inputName in arg.providedInputs) {
                this._providedInputs[inputName] = arg.providedInputs[inputName];
            }
        }
    }

    get definition(): OperationDefinition {
        return this._definition;
    }

    get providedInputs(): Record<string, ProvidedInput> {
        return this._providedInputs;
    }

    get hasEmptyInputs() : boolean {
        return Object.getOwnPropertyNames(this._providedInputs).length < this.definition.inputNames.length;
    }

    abstract renderOutput(name: string) : string;

    abstract clone() : AbstractOperation;
}

export abstract class AbstractCallerOperation extends AbstractOperation {

    private _providedOutcomes: Record<string, ProvidedOutcome> = {};

    constructor(definition: CallerOperationDefinition)
    constructor(source: AbstractCallerOperation)
    constructor(arg: CallerOperationDefinition | AbstractCallerOperation)
    constructor(arg: CallerOperationDefinition | AbstractCallerOperation) {
        super(arg);
        if(arg instanceof AbstractCallerOperation) {
            for(let outcomeName in arg.providedOutcomes) {
                this.providedOutcomes[outcomeName] = arg.providedOutcomes[outcomeName];
            }
        }
    }

    get providedOutcomes(): Record<string, ProvidedOutcome> {
        return this._providedOutcomes;
    }

    get definition(): CallerOperationDefinition {
        return super.definition as CallerOperationDefinition;
    }

    protected renderOutcomeIfExists(outcome: string, indent: number) {
        if(this.providedOutcomes[outcome]) {
            return this.providedOutcomes[outcome].render(indent);
        } else {
            return "";
        }
    }

    abstract renderCall(indent: number) : string;

    abstract clone() : AbstractCallerOperation;
}

export abstract class AbstractStartOperation extends AbstractCallerOperation {

    constructor(definition: AbstractStartDefinition)
    constructor(source: AbstractStartOperation)
    constructor(arg: AbstractStartDefinition | AbstractStartOperation) {
        super(arg);
    }

    protected renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    abstract clone() : AbstractStartOperation;
}
