import {AbstractBinaryOperator} from "@/model/operation/library/operator/AbstractBinaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export enum BinaryArithmeticOperators {
    ADD = "+",
    SUBTRACT = "-",
    MULTIPLY = "*",
    DIVIDE = "/"
}

export class BinaryArithmeticOperator extends AbstractBinaryOperator {

    private _operator: BinaryArithmeticOperators;

    constructor(source: BinaryArithmeticOperator)
    constructor(operator: BinaryArithmeticOperators)
    constructor(arg: BinaryArithmeticOperators | BinaryArithmeticOperator) {
        if(arg instanceof BinaryArithmeticOperator) {
            super(arg);
            this._operator = arg.operator;
        } else {
            super(arg, DataType.NUMBER, DataCollection.SINGLE, DataType.NUMBER, DataCollection.SINGLE);
            this._operator = arg;
        }
    }

    get operator(): BinaryArithmeticOperators {
        return this._operator;
    }

    renderOutput(output: string): string {
        let left = this.renderLeftInput();
        let right = this.renderRightInput();

        switch (this._operator) {
            case BinaryArithmeticOperators.ADD:
                return "$((" + left + " + " + right + "))";
            case BinaryArithmeticOperators.SUBTRACT:
                return "$((" + left + " - " + right + "))";
            case BinaryArithmeticOperators.MULTIPLY:
                return "$((" + left + " * " + right + "))";
            case BinaryArithmeticOperators.DIVIDE:
                return "$((" + left + " / " + right + "))";
        }
    }

    clone(): BinaryArithmeticOperator {
        return new BinaryArithmeticOperator(this);
    }

}
