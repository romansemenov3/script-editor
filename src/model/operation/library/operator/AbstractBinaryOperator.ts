import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {AbstractOperation} from "@/model/operation/Operation";

export abstract class AbstractBinaryOperator extends AbstractOperation {

    constructor(source: AbstractBinaryOperator)
    constructor(name: string,
                inputType: DataType, inputCollection: DataCollection,
                outputType: DataType, outputCollection: DataCollection)
    constructor(arg: string | AbstractBinaryOperator,
                inputType?: DataType, inputCollection?: DataCollection,
                outputType?: DataType, outputCollection?: DataCollection) {
        if(arg instanceof AbstractBinaryOperator) {
            super(arg);
        } else if(inputType && inputCollection && outputType && outputCollection) {
            super(DefinitionBuilder.common(arg || "AbstractBinaryOperator")
                .addInput("Left", inputType, inputCollection)
                .addInput("Right", inputType, inputCollection)
                .addOutput("Value", outputType, outputCollection)
                .build()
            );
        } else {
            throw new Error("Invalid arguments");
        }
    }

    protected renderLeftInput() : string {
        return this.providedInputs["Left"].rendered;
    }

    protected renderRightInput() : string {
        return this.providedInputs["Right"].rendered;
    }

}
