import {AbstractOperation} from "@/model/operation/Operation";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";

export abstract class AbstractUnaryOperator extends AbstractOperation {

    constructor(source: AbstractUnaryOperator)
    constructor(name: string,
                inputType: DataType, inputCollection: DataCollection,
                outputType: DataType, outputCollection: DataCollection)
    constructor(arg: string | AbstractUnaryOperator,
                inputType?: DataType, inputCollection?: DataCollection,
                outputType?: DataType, outputCollection?: DataCollection) {
        if(arg instanceof AbstractUnaryOperator) {
            super(arg);
        } else if(inputType && inputCollection && outputType && outputCollection) {
            super(DefinitionBuilder.common(arg || "AbstractUnaryOperator")
                .addInput("Value", inputType, inputCollection)
                .addOutput("Value", outputType, outputCollection)
                .build());
        } else {
            throw new Error("Invalid arguments");
        }
    }

    protected renderValueInput() : string {
        return this.providedInputs["Value"].rendered;
    }

}
