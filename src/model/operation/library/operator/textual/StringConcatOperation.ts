import {AbstractBinaryOperator} from "@/model/operation/library/operator/AbstractBinaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class StringConcatOperation extends AbstractBinaryOperator {

    constructor(source: StringConcatOperation)
    constructor()
    constructor(arg?: StringConcatOperation) {
        if(arg instanceof StringConcatOperation) {
            super(arg);
        } else {
            super("Concat", DataType.STRING, DataCollection.SINGLE, DataType.STRING, DataCollection.SINGLE);
        }
    }

    renderOutput(name: string): string {
        return this.renderLeftInput() + this.renderRightInput();
    }

    clone(): StringConcatOperation {
        return new StringConcatOperation(this);
    }

}
