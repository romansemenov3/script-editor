import {AbstractBinaryOperator} from "@/model/operation/library/operator/AbstractBinaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export abstract class AbstractBinaryLogicalOperator extends AbstractBinaryOperator {

    constructor(source: AbstractBinaryLogicalOperator)
    constructor(name: string, inputType: DataType, inputCollection: DataCollection)
    constructor(arg: string | AbstractBinaryLogicalOperator, inputType?: DataType, inputCollection?: DataCollection) {
        if(arg instanceof AbstractBinaryLogicalOperator) {
            super(arg);
        } else if(inputType && inputCollection) {
            super(arg || "AbstractBinaryLogicalOperator", inputType, inputCollection, DataType.BOOLEAN, DataCollection.SINGLE);
        } else {
            throw new Error("Invalid arguments");
        }
    }

}


