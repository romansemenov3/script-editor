import {AbstractBinaryLogicalOperator} from "@/model/operation/library/operator/logical/AbstractBinaryLogicalOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export enum BinaryLogicalOperators {
    AND = "&&",
    OR = "||"
}

export class BinaryLogicalOperator extends AbstractBinaryLogicalOperator {

    private readonly _operator: BinaryLogicalOperators;

    constructor(source: BinaryLogicalOperator)
    constructor(operator: BinaryLogicalOperators)
    constructor(arg: BinaryLogicalOperators | BinaryLogicalOperator) {
        if(arg instanceof BinaryLogicalOperator) {
            super(arg);
            this._operator = arg.operator;
        } else {
            super(arg, DataType.BOOLEAN, DataCollection.SINGLE);
            this._operator = arg;
        }

    }

    get operator(): BinaryLogicalOperators {
        return this._operator;
    }

    renderOutput(output: string): string {
        let left = this.renderLeftInput();
        let right = this.renderRightInput();

        switch (this._operator) {
            case BinaryLogicalOperators.AND:
                return "(" + left + " && " + right + ")";
            case BinaryLogicalOperators.OR:
                return "(" + left + " || " + right + ")";
        }
    }

    clone(): BinaryLogicalOperator {
        return new BinaryLogicalOperator(this);
    }

}
