import {AbstractBinaryLogicalOperator} from "@/model/operation/library/operator/logical/AbstractBinaryLogicalOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export enum ComparisonOperators {
    EQUALS = "==",
    NOT_EQUALS = "!=",
    LESS_THAN = "<",
    LESS_THAN_EQUALS = "<=",
    GREATER_THAN = ">",
    GREATER_THAN_EQUALS = ">="
}

export class ComparisonOperator extends AbstractBinaryLogicalOperator {

    private readonly _operator: ComparisonOperators;

    constructor(source: ComparisonOperator)
    constructor(operator: ComparisonOperators, inputType: DataType, inputCollection?: DataCollection)
    constructor(arg: ComparisonOperators | ComparisonOperator, inputType?: DataType, inputCollection?: DataCollection) {
        if(arg instanceof ComparisonOperator) {
            super(arg);
            this._operator = arg.operator;
        } else if(inputType) {
            super(arg, inputType, inputCollection || DataCollection.SINGLE);
            this._operator = arg;
        } else {
            throw new Error("Invalid arguments");
        }
    }

    get operator(): ComparisonOperators {
        return this._operator;
    }

    renderOutput(output: string): string {
        let left = this.renderLeftInput();
        let right = this.renderRightInput();

        switch (this._operator) {
            case ComparisonOperators.EQUALS:
                return "(" + left + " -eq " + right + ")";
            case ComparisonOperators.NOT_EQUALS:
                return "(" + left + " -ne " + right + ")";
            case ComparisonOperators.GREATER_THAN:
                return "(" + left + " -gt " + right + ")";
            case ComparisonOperators.LESS_THAN:
                return "(" + left + " -lt " + right + ")";
            case ComparisonOperators.GREATER_THAN_EQUALS:
                return "(" + left + " -ge " + right + ")";
            case ComparisonOperators.LESS_THAN_EQUALS:
                return "(" + left + " -le " + right + ")";
        }
    }

    clone(): ComparisonOperator {
        return new ComparisonOperator(this);
    }

}
