import {AbstractUnaryOperator} from "@/model/operation/library/operator/AbstractUnaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export enum UnaryLogicalOperators {
    NOT = "!"
}

export class UnaryLogicalOperator extends AbstractUnaryOperator {

    private readonly _operator: UnaryLogicalOperators;

    constructor(source: UnaryLogicalOperator)
    constructor(operator: UnaryLogicalOperators)
    constructor(arg: UnaryLogicalOperators | UnaryLogicalOperator) {
        if(arg instanceof UnaryLogicalOperator) {
            super(arg);
            this._operator = arg.operator;
        } else {
            super(arg, DataType.BOOLEAN, DataCollection.SINGLE, DataType.BOOLEAN, DataCollection.SINGLE);
            this._operator = arg;
        }
    }

    get operator(): UnaryLogicalOperators {
        return this._operator;
    }

    renderOutput(output: string): string {
        let value = this.renderValueInput();
        switch (this._operator) {
            case UnaryLogicalOperators.NOT:
                return "(!" + value + ")";
        }
    }

    clone(): UnaryLogicalOperator {
        return new UnaryLogicalOperator(this);
    }

}
