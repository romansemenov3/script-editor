import {AbstractVariableSetter} from "@/model/operation/library/callable/setter/AbstractVariableSetter";
import {StringUtils} from "@/service/StringUtils";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class NumberVariableSetter extends AbstractVariableSetter {

    constructor(source: NumberVariableSetter)
    constructor(variable: string)
    constructor(arg: NumberVariableSetter | string) {
        if(arg instanceof NumberVariableSetter) {
            super(arg);
        } else {
            super(arg, DataType.NUMBER, DataCollection.SINGLE);
        }
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + this.variable + "=$((" + this.renderInputValue() + "))\n";
        return command + this.renderAfterOutcome(indent);
    }

    clone(): NumberVariableSetter {
        return new NumberVariableSetter(this);
    }

}
