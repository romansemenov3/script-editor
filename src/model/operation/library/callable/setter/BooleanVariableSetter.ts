import {AbstractVariableSetter} from "@/model/operation/library/callable/setter/AbstractVariableSetter";
import {StringUtils} from "@/service/StringUtils";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class BooleanVariableSetter extends AbstractVariableSetter {

    constructor(source: BooleanVariableSetter)
    constructor(variable: string)
    constructor(arg: BooleanVariableSetter | string) {
        if(arg instanceof BooleanVariableSetter) {
            super(arg);
        } else {
            super(arg, DataType.BOOLEAN, DataCollection.SINGLE);
        }
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + this.variable + "=$((" + this.renderInputValue() + "))\n";
        return command + this.renderAfterOutcome(indent);
    }

    clone() : BooleanVariableSetter {
        return new BooleanVariableSetter(this);
    }

}
