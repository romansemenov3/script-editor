import {AbstractCallerOperation} from "@/model/operation/Operation";
import {DataCollection, DataType, RelativeScope} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";

export abstract class AbstractVariableSetter extends AbstractCallerOperation {

    private _variable: string;

    constructor(source: AbstractVariableSetter)
    constructor(variable: string, type: DataType, collection: DataCollection)
    constructor(arg: string | AbstractVariableSetter, type?: DataType, collection?: DataCollection) {
        if(arg instanceof AbstractVariableSetter) {
            super(arg);
            this._variable = arg.variable;
        } else if(type && collection) {
            super(DefinitionBuilder.callable("Set " + arg)
                .addInput("Value", type, collection)
                .addOutput("Value", type, collection)
                .addOutcome("After", RelativeScope.CURRENT)
                .build()
            );
            this._variable = arg;
        } else {
            throw new Error("Invalid arguments");
        }
    }

    get variable(): string {
        return this._variable;
    }

    set variable(value: string) {
        this._variable = value;
    }

    protected renderInputValue() : string {
        return this.providedInputs["Value"].rendered;
    }

    protected renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    renderOutput(output: string): string {
        return "$" + this._variable;
    }

}
