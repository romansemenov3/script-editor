import {AbstractVariableSetter} from "@/model/operation/library/callable/setter/AbstractVariableSetter";
import {StringUtils} from "@/service/StringUtils";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class StringVariableSetter extends AbstractVariableSetter {

    constructor(source: StringVariableSetter)
    constructor(variable: string)
    constructor(arg: StringVariableSetter | string) {
        if(arg instanceof StringVariableSetter) {
            super(arg);
        } else {
            super(arg, DataType.STRING, DataCollection.SINGLE);
        }
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + this.variable + "=" + this.renderInputValue() + "\n";
        return command + this.renderAfterOutcome(indent);
    }

    clone(): StringVariableSetter {
        return new StringVariableSetter(this);
    }

}
