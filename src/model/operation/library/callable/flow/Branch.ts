import {UndefinedOutputError} from "@/model/error/UndefinedOutputError";
import {StringUtils} from "@/service/StringUtils";
import {AbstractCallerOperation} from "@/model/operation/Operation";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {DataType, RelativeScope} from "@/model/definition/DataType";

export class Branch extends AbstractCallerOperation {

    constructor()
    constructor(source: Branch)
    constructor(arg?: Branch) {
        if(arg instanceof Branch) {
            super(arg);
        } else {
            super(DefinitionBuilder.callable("If")
                .addSingleInput("Condition", DataType.BOOLEAN)
                .addOutcome("After", RelativeScope.CURRENT)
                .addOutcome("True", RelativeScope.CHILD)
                .addOutcome("False", RelativeScope.CHILD)
                .build()
            );
        }
    }

    renderOutput(output: string): string {
        throw new UndefinedOutputError(this, output);
    }

    private renderConditionInput() : string {
        return this.providedInputs["Condition"].rendered;
    }

    private renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    private renderTrueOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("True", indent + 1);
    }

    private renderFalseOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("False", indent + 1);
    }

    renderCall(indent: number): string {
        let command = "";
        if(this.providedOutcomes["True"]) {
            command += StringUtils.indent(indent) + "if [[ " + this.renderConditionInput() + " ]]\n";
            command += StringUtils.indent(indent) + "then\n";
            command += this.renderTrueOutcome(indent);

            if(this.providedOutcomes["False"]) {
                command += StringUtils.indent(indent) + "else\n";
                command += this.renderFalseOutcome(indent);
            }
            command += StringUtils.indent(indent) + "fi\n";
        } else {
            if(this.providedOutcomes["False"]) {
                command += StringUtils.indent(indent) + "if [[ !(" + this.renderConditionInput() + ") ]]\n";
                command += StringUtils.indent(indent) + "then\n";
                command += this.renderFalseOutcome(indent);
                command += StringUtils.indent(indent) + "fi\n";
            }
        }
        command += this.renderAfterOutcome(indent);
        return command;
    }

    clone(): Branch {
        return new Branch(this);
    }
}
