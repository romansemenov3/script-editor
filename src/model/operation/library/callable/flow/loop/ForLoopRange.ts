import {AbstractCallerOperation} from "@/model/operation/Operation";
import {StringUtils} from "@/service/StringUtils";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {DataType, RelativeScope} from "@/model/definition/DataType";

export class ForLoopRange extends AbstractCallerOperation {

    private readonly _index: string;

    constructor()
    constructor(source: ForLoopRange)
    constructor(arg?: ForLoopRange) {
        if(arg instanceof ForLoopRange) {
            super(arg);
        } else {
            super(DefinitionBuilder.callable("For Range")
                .addSingleInput("From", DataType.NUMBER)
                .addSingleInput("To", DataType.NUMBER)
                .addSingleOutput("Index", DataType.NUMBER)
                .addOutcome("After", RelativeScope.CURRENT)
                .addOutcome("Body", RelativeScope.CHILD)
                .build()
            );
        }
        this._index = StringUtils.generateAlias() + "_INDEX";
    }

    private renderFromInput() : string {
        return this.providedInputs["From"].rendered;
    }

    private renderToInput() : string {
        return this.providedInputs["To"].rendered;
    }

    private renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    private renderBodyOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("Body", indent + 1);
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + "for " + this._index + " in $(seq " + this.renderFromInput() + " " + this.renderToInput() +")\n";
        command += StringUtils.indent(indent) + "do\n";
        command += this.renderBodyOutcome(indent);
        command += StringUtils.indent(indent) + "done\n";
        command += this.renderAfterOutcome(indent);
        return command;
    }

    renderOutput(name: string): string {
        return "$" + this._index;
    }

    clone(): ForLoopRange {
        return new ForLoopRange(this);
    }
}
