import {AbstractCallerOperation} from "@/model/operation/Operation";
import {StringUtils} from "@/service/StringUtils";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {DataCollection, DataType, RelativeScope} from "@/model/definition/DataType";

export class ForEachLoop extends AbstractCallerOperation {

    private readonly _element: string;

    constructor(source: ForEachLoop)
    constructor(type: DataType)
    constructor(arg: ForEachLoop | DataType) {
        if(arg instanceof ForEachLoop) {
            super(arg);
        } else {
            super(DefinitionBuilder.callable("For Each")
                .addInput("Array", arg, DataCollection.ARRAY)
                .addSingleOutput("Element", arg)
                .addOutcome("After", RelativeScope.CURRENT)
                .addOutcome("Body", RelativeScope.CHILD)
                .build()
            );
        }
        this._element = StringUtils.generateAlias() + "_ELEMENT";
    }

    private renderArrayInput() : string {
        return this.providedInputs["Array"].rendered;
    }

    private renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    private renderBodyOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("Body", indent + 1);
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + "for " + this._element + " in \"${" + this.renderArrayInput() + "[@]}\"\n";
        command += StringUtils.indent(indent) + "do\n";
        command += this.renderBodyOutcome(indent);
        command += StringUtils.indent(indent) + "done\n";
        command += this.renderAfterOutcome(indent);
        return command;
    }

    renderOutput(name: string): string {
        return "$" + this._element;
    }

    clone(): ForEachLoop {
        return new ForEachLoop(this);
    }
}
