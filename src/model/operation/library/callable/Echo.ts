import {StringUtils} from "@/service/StringUtils";
import {UndefinedOutputError} from "@/model/error/UndefinedOutputError";
import {AbstractCallerOperation} from "@/model/operation/Operation";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {DataType, RelativeScope} from "@/model/definition/DataType";

export class Echo extends AbstractCallerOperation {

    constructor(source: Echo)
    constructor()
    constructor(arg?: Echo) {
        if(arg instanceof Echo) {
            super(arg);
        } else {
            super(DefinitionBuilder.callable("Echo")
                .addSingleInput("Message", DataType.STRING)
                .addOutcome("After", RelativeScope.CURRENT)
                .build()
            );
        }
    }

    private renderMessageInput() : string {
        return this.providedInputs["Message"].rendered;
    }

    private renderAfterOutcome(indent: number) : string {
        return this.renderOutcomeIfExists("After", indent);
    }

    renderOutput(output: string): string {
        throw new UndefinedOutputError(this, output);
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + "echo " + this.renderMessageInput() + "\n";
        return command + this.renderAfterOutcome(indent);
    }

    clone(): Echo {
        return new Echo(this);
    }

}
