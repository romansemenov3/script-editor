import {StringUtils} from "@/service/StringUtils";
import {AbstractStartOperation} from "@/model/operation/Operation";
import {AbstractStartDefinition} from "@/model/definition/AbstractStartDefinition";
import {OutputDefinition} from "@/model/definition/Definition";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class StartDefinition extends AbstractStartDefinition {

    constructor() {
        super({
            "Args": new OutputDefinition(DataType.STRING, DataCollection.ARRAY)
        });
    }
}

export class Start extends AbstractStartOperation {

    constructor(source: Start)
    constructor()
    constructor(arg?: Start) {
        if(arg instanceof Start) {
            super(arg);
        } else {
            super(new StartDefinition());
        }
    }

    renderOutput(output: string): string {
        return "ARGS_ARRAY";
    }

    renderCall(indent: number): string {
        let command = StringUtils.indent(indent) + "#!/bin/bash\n";
        command += StringUtils.indent(indent) + "ARGS_ARRAY=($@)\n";
        return command + this.renderAfterOutcome(indent);
    }

    clone(): Start {
        return new Start(this);
    }

}
