import {VariableGetter} from "@/model/operation/library/getter/VariableGetter";
import {AbstractVariableSetter} from "@/model/operation/library/callable/setter/AbstractVariableSetter";
import {StringVariableSetter} from "@/model/operation/library/callable/setter/StringVariableSetter";
import {NumberVariableSetter} from "@/model/operation/library/callable/setter/NumberVariableSetter";
import {BooleanVariableSetter} from "@/model/operation/library/callable/setter/BooleanVariableSetter";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class Variable {

    private _name: string;
    private _type: DataType;
    private _collection: DataCollection;

    private _getters: VariableGetter[] = [];
    private _setters: AbstractVariableSetter[] = [];

    constructor(name: string, type: DataType, collection: DataCollection = DataCollection.SINGLE) {
        this._name = name;
        this._type = type;
        this._collection = collection;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
        this._getters.forEach(getter => getter.variable = value);
        this._setters.forEach(setter => setter.variable = value);
    }

    get type(): DataType {
        return this._type;
    }

    get collection(): DataCollection {
        return this._collection;
    }

    get getters(): VariableGetter[] {
        return this._getters;
    }

    get setters(): AbstractVariableSetter[] {
        return this._setters;
    }

    generateGetter(): VariableGetter {
        return new VariableGetter(this._name, this._type, this._collection);
    }

    generateSetter(): AbstractVariableSetter {
        switch (this._type) {
            case DataType.STRING:
                return new StringVariableSetter(this._name);
            case DataType.NUMBER:
                return new NumberVariableSetter(this._name);
            case DataType.BOOLEAN:
                return new BooleanVariableSetter(this._name);
        }
    }

}
