import {AbstractUnaryOperator} from "@/model/operation/library/operator/AbstractUnaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";


export class StringToNumberConverter extends AbstractUnaryOperator {

    constructor(source: StringToNumberConverter)
    constructor()
    constructor(arg?: StringToNumberConverter) {
        if(arg instanceof StringToNumberConverter) {
            super(arg);
        } else {
            super("To Number", DataType.STRING, DataCollection.SINGLE, DataType.NUMBER, DataCollection.SINGLE);
        }
    }

    renderOutput(output: string): string {
        return "$((" + this.renderValueInput() + "))";
    }

    clone(): StringToNumberConverter {
        return new StringToNumberConverter(this);
    }

}
