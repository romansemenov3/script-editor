import {AbstractUnaryOperator} from "@/model/operation/library/operator/AbstractUnaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";


export class NumberToStringConverter extends AbstractUnaryOperator {

    constructor(source: NumberToStringConverter)
    constructor()
    constructor(arg?: NumberToStringConverter) {
        if(arg instanceof NumberToStringConverter) {
            super(arg);
        } else {
            super("To String", DataType.NUMBER, DataCollection.SINGLE, DataType.STRING, DataCollection.SINGLE);
        }
    }

    renderOutput(output: string): string {
        return "$((" + this.renderValueInput() + "))";
    }

    clone(): NumberToStringConverter {
        return new NumberToStringConverter(this);
    }

}
