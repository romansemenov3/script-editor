import {DataCollection, DataType} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {AbstractOperation} from "@/model/operation/Operation";

export class ConstantGetter extends AbstractOperation {

    private readonly _value: string;
    private readonly _type: DataType;

    constructor(source: ConstantGetter)
    constructor(value: string, type: DataType, collection?: DataCollection)
    constructor(arg: string | ConstantGetter, type?: DataType, collection?: DataCollection) {
        if(arg instanceof ConstantGetter) {
            super(arg);
            this._value = arg.value;
            this._type = arg.type;
        } else if(type) {
            super(DefinitionBuilder.common("Constant")
                .addOutput("Value", type, collection || DataCollection.SINGLE)
                .build()
            );
            this._value = arg;
            this._type = type;
        } else {
            throw new Error("Invalid arguments");
        }
    }

    get type(): DataType {
        return this._type;
    }

    get value(): string {
        return this._value;
    }

    renderOutput(output: string): string {
        switch (this._type) {
            case DataType.BOOLEAN:
                return this._value;
            case DataType.NUMBER:
                return this._value;
            case DataType.STRING:
                return "\"" + this._value
                    .replaceAll("\\", "\\\\")
                    .replaceAll("\"", "\\\"")
                + "\"";
        }
    }

    clone(): ConstantGetter {
        return new ConstantGetter(this);
    }

}
