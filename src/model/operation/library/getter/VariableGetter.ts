import {AbstractOperation} from "@/model/operation/Operation";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";

export class VariableGetter extends AbstractOperation {

    private _variable: string;

    constructor(source: VariableGetter)
    constructor(variable: string, type: DataType, collection: DataCollection)
    constructor(arg: string | VariableGetter, type?: DataType, collection?: DataCollection) {
        if(arg instanceof VariableGetter) {
            super(arg);
            this._variable = arg.variable;
        } else if(type && collection) {
            super(DefinitionBuilder.common("Get " + arg)
                .addOutput("Value", type, collection)
                .build()
            );
            this._variable = arg;
        } else {
            throw new Error("Invalid arguments");
        }
    }

    get variable(): string {
        return this._variable;
    }

    set variable(value: string) {
        this._variable = value;
    }

    renderOutput(output: string): string {
        return "$" + this._variable;
    }

    clone(): VariableGetter {
        return new VariableGetter(this);
    }

}
