import {AbstractOperation} from "@/model/operation/Operation";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";

export class ArrayLengthGetter extends AbstractOperation {

    constructor(source: ArrayLengthGetter)
    constructor(type: DataType)
    constructor(arg: DataType | ArrayLengthGetter) {
        if(arg instanceof ArrayLengthGetter) {
            super(arg);
        } else {
            super(DefinitionBuilder.common("Length")
                .addInput("Array", arg, DataCollection.ARRAY)
                .addSingleOutput("Value", arg)
                .build()
            );
        }
    }

    private renderArrayInput() : string {
        return this.providedInputs["Array"].rendered;
    }

    private renderIndexInput() : string {
        return this.providedInputs["Index"].rendered;
    }

    renderOutput(name: string): string {
        return "${#" + this.renderArrayInput() + "[*]}";
    }

    clone(): ArrayLengthGetter {
        return new ArrayLengthGetter(this);
    }

}
