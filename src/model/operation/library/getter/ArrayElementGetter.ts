import {AbstractOperation} from "@/model/operation/Operation";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";

export class ArrayElementGetter extends AbstractOperation {

    constructor(source: ArrayElementGetter)
    constructor(type: DataType)
    constructor(arg: DataType | ArrayElementGetter) {
        if(arg instanceof ArrayElementGetter) {
            super(arg);
        } else {
            super(DefinitionBuilder.common("Get")
                .addInput("Array", arg, DataCollection.ARRAY)
                .addSingleInput("Index", DataType.NUMBER)
                .addSingleOutput("Value", arg)
                .build()
            );
        }
    }

    private renderArrayInput() : string {
        return this.providedInputs["Array"].rendered;
    }

    private renderIndexInput() : string {
        return this.providedInputs["Index"].rendered;
    }

    renderOutput(name: string): string {
        return "${" + this.renderArrayInput() + "[" + this.renderIndexInput() + "]}";
    }

    clone(): ArrayElementGetter {
        return new ArrayElementGetter(this);
    }

}
