import {AbstractUnaryOperator} from "@/model/operation/library/operator/AbstractUnaryOperator";
import {DataCollection, DataType} from "@/model/definition/DataType";

export class ValuePass extends AbstractUnaryOperator {

    constructor(source: ValuePass)
    constructor(type: DataType, collection: DataCollection)
    constructor(arg: DataType | ValuePass, collection?: DataCollection) {
        if(arg instanceof ValuePass) {
            super(arg);
        } else if(collection) {
            super("Value Pass", arg, collection, arg, collection);
        } else {
            throw new Error("Invalid arguments");
        }
    }

    renderOutput(name: string): string {
        return this.renderValueInput();
    }

    clone(): ValuePass {
        return new ValuePass(this);
    }

}
