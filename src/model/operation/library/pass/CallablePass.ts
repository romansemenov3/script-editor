import {UndefinedOutputError} from "@/model/error/UndefinedOutputError";
import {AbstractCallerOperation} from "@/model/operation/Operation";
import {DefinitionBuilder} from "@/model/definition/DefinitionBuilder";
import {RelativeScope} from "@/model/definition/DataType";

export class CallablePass extends AbstractCallerOperation {

    constructor(source: CallablePass)
    constructor()
    constructor(arg?: CallablePass) {
        if(arg instanceof CallablePass) {
            super(arg);
        } else {
            super(DefinitionBuilder.callable("Callable Pass")
                .addOutcome("After", RelativeScope.CURRENT)
                .build()
            );
        }
    }

    renderCall(indent: number): string {
        return this.renderOutcomeIfExists("After", indent);
    }

    renderOutput(name: string): string {
        throw new UndefinedOutputError(this, name);
    }

    clone(): CallablePass {
        return new CallablePass(this);
    }

}
