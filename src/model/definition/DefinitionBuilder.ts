import {DataCollection, DataType, RelativeScope} from "@/model/definition/DataType";
import {
    CallableOperationDefinition,
    InputDefinition,
    OperationDefinition,
    OutcomeDefinition,
    OutputDefinition
} from "@/model/definition/Definition";

export abstract class DefinitionBuilder {

    static common(name: string) : CommonDefinitionBuilder {
        return new CommonDefinitionBuilder(name);
    }

    static callable(name: string) : CallableDefinitionBuilderImpl {
        return new CallableDefinitionBuilderImpl(name);
    }

}

export class CommonDefinitionBuilder {

    protected _name: string;
    protected _inputs: Record<string, InputDefinition> = {};
    protected _outputs: Record<string, OutputDefinition> = {};

    constructor(name: string) {
        this._name = name;
    }

    addInput(name: string, type: DataType, collection: DataCollection): CommonDefinitionBuilder {
        this._inputs[name] = new InputDefinition(type, collection);
        return this;
    }

    addOutput(name: string, type: DataType, collection: DataCollection): CommonDefinitionBuilder {
        this._outputs[name] = new OutputDefinition(type, collection);
        return this;
    }

    addSingleOutput(name: string, type: DataType): CommonDefinitionBuilder {
        return this.addOutput(name, type, DataCollection.SINGLE);
    }

    addSingleInput(name: string, type: DataType): CommonDefinitionBuilder {
        return this.addInput(name, type, DataCollection.SINGLE);
    }

    build(): OperationDefinition {
        return new OperationDefinition(this._name, this._inputs, this._outputs);
    }
}

export class CallableDefinitionBuilderImpl extends CommonDefinitionBuilder {

    protected _outcomes: Record<string, OutcomeDefinition> = {};

    addInput(name: string, type: DataType, collection: DataCollection): CallableDefinitionBuilderImpl {
        return super.addInput(name, type, collection) as CallableDefinitionBuilderImpl;
    }

    addOutput(name: string, type: DataType, collection: DataCollection): CallableDefinitionBuilderImpl {
        return super.addOutput(name, type, collection) as CallableDefinitionBuilderImpl;
    }

    addSingleOutput(name: string, type: DataType): CallableDefinitionBuilderImpl {
        return super.addSingleOutput(name, type) as CallableDefinitionBuilderImpl;
    }

    addSingleInput(name: string, type: DataType): CallableDefinitionBuilderImpl {
        return super.addSingleInput(name, type) as CallableDefinitionBuilderImpl;
    }

    addOutcome(name: string, calledIn: RelativeScope): CallableDefinitionBuilderImpl {
        this._outcomes[name] = new OutcomeDefinition(calledIn);
        return this;
    }

    build(): CallableOperationDefinition {
        return new CallableOperationDefinition(this._name, this._inputs, this._outputs, this._outcomes);
    }
}
