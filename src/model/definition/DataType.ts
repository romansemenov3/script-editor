export enum DataType {
    BOOLEAN = "Boolean",
    NUMBER = "Number",
    STRING = "String"
}

export enum DataCollection {
    SINGLE = "Single",
    ARRAY = "Array"
}

export enum RelativeScope {
    CURRENT = "Current",
    CHILD = "Child"
}
