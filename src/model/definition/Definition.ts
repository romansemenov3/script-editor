import {DataCollection, DataType, RelativeScope} from "@/model/definition/DataType";

export class InputDefinition {
    private readonly _type: DataType;
    private readonly _collection: DataCollection;

    constructor(type: DataType, collection: DataCollection) {
        this._type = type;
        this._collection = collection;
    }

    get type(): DataType {
        return this._type;
    }

    get collection(): DataCollection {
        return this._collection;
    }

    public accepts(output: OutputDefinition) : boolean {
        return this.type === output.type && this.collection === output.collection;
    }
}

export class OutputDefinition {
    private readonly _type: DataType;
    private readonly _collection: DataCollection;

    constructor(type: DataType, collection: DataCollection) {
        this._type = type;
        this._collection = collection;
    }

    get type(): DataType {
        return this._type;
    }

    get collection(): DataCollection {
        return this._collection;
    }
}

export class OutcomeDefinition {
    private readonly _calledIn: RelativeScope;

    constructor(calledIn: RelativeScope) {
        this._calledIn = calledIn;
    }

    get calledIn(): RelativeScope {
        return this._calledIn;
    }
}

export class OperationDefinition {
    private readonly _name: string;
    private _inputs: Record<string, InputDefinition>;
    private _outputs: Record<string, OutputDefinition>;

    constructor(name: string, inputs: Record<string, InputDefinition>, outputs: Record<string, OutputDefinition>) {
        this._name = name;
        this._inputs = inputs;
        this._outputs = outputs;
    }

    get name(): string {
        return this._name;
    }

    get inputs(): Record<string, InputDefinition> {
        return this._inputs;
    }

    get outputs(): Record<string, OutputDefinition> {
        return this._outputs;
    }

    get inputNames(): string[] {
        return Object.getOwnPropertyNames(this._inputs);
    }

    get outputNames(): string[] {
        return Object.getOwnPropertyNames(this._outputs);
    }

    get isRoot() : boolean {
        return this.inputNames.length === 0;
    }
}

export class CallerOperationDefinition extends OperationDefinition {

    private _outcomes: Record<string, OutcomeDefinition>;

    constructor(name: string,
                inputs: Record<string, InputDefinition>,
                outputs: Record<string, OutputDefinition>,
                outcomes: Record<string, OutcomeDefinition>) {
        super(name, inputs, outputs);
        this._outcomes = outcomes;
    }

    get outcomes(): Record<string, OutcomeDefinition> {
        return this._outcomes;
    }

    get outcomeNames() : string[] {
        return Object.getOwnPropertyNames(this._outcomes);
    }

    get outcomeAndOutputNames() : string[] {
        return this.outcomeNames.concat(this.outputNames);
    }
}

export class CallableOperationDefinition extends CallerOperationDefinition {
    get isRoot() : boolean {
        return false;
    }
}
