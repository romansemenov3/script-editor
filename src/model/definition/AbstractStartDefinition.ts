import {CallerOperationDefinition, OutcomeDefinition, OutputDefinition} from "@/model/definition/Definition";
import {RelativeScope} from "@/model/definition/DataType";

export class AbstractStartDefinition extends CallerOperationDefinition {

    constructor(outputs: Record<string, OutputDefinition>) {
        super("Start", {}, outputs, {"After": new OutcomeDefinition(RelativeScope.CURRENT)});
    }
}
