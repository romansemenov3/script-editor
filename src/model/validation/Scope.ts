import {ScopesAreNotRelativesError} from "@/model/error/ScopesAreNotRelativesError";

export class Scope {

    private _parent: Scope | null;

    constructor(parent: Scope | null = null) {
        this._parent = parent;
    }

    get parent(): Scope | null {
        return this._parent;
    }

    set parent(value: Scope | null) {
        this._parent = value;
    }

    equalsOrChildOf(ancestor: Scope) : boolean {
        if(ancestor === this) {
            return true;
        }

        let currentAncestor = this._parent;
        while(currentAncestor) {
            if(currentAncestor === ancestor) {
                return true;
            }
            currentAncestor = currentAncestor.parent;
        }
        return false;
    }

    static findYoungest(relatives: Scope[]) : Scope {
        if(relatives === undefined || relatives.length === 0) {
            throw new Error("Illegal relatives value: " + relatives);
        }
        switch (relatives.length) {
            case 0:
                throw new Error("Illegal relatives value: " + relatives);
            case 1:
                return relatives[0];
            case 2:
                if(relatives[0].equalsOrChildOf(relatives[1])) {
                    return relatives[0];
                } else if (relatives[1].equalsOrChildOf(relatives[0])) {
                    return relatives[1];
                } else {
                    throw new ScopesAreNotRelativesError();
                }
            default: {
                let result = Scope.findYoungest([relatives[0], relatives[1]]);
                for (let i = 2; i < relatives.length && result; i++) {
                    result = Scope.findYoungest([result, relatives[i]]);
                }
                return result;
            }
        }
    }

}
