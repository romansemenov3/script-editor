import {AbstractOperation} from "@/model/operation/Operation";

export class InvalidOperation {
    private readonly _operation: AbstractOperation;
    private _errors: Error[] = [];

    constructor(operation: AbstractOperation) {
        this._operation = operation;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }

    get errors(): Error[] {
        return this._errors;
    }
}

export class ValidationErrors {
    private _invalidOperations: InvalidOperation[] = [];

    get invalidOperations(): InvalidOperation[] {
        return this._invalidOperations;
    }

    get hasErrors() : boolean {
        return this._invalidOperations.length > 0;
    }

    getOperationErrors(operation: AbstractOperation) : InvalidOperation | null {
        for(let i = 0; i < this._invalidOperations.length; i++) {
            if(this._invalidOperations[i].operation === operation) {
                return this._invalidOperations[i];
            }
        }
        return null;
    }

    addValidationError(operation: AbstractOperation, error: Error) {
        let target = this.getOperationErrors(operation);
        if(target === null) {
            target = new InvalidOperation(operation);
            this.invalidOperations.push(target);
        }
        target.errors.push(error);
    }
}
