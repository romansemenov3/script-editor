import {ValidationErrors} from "@/model/validation/ValidationError";
import {AbstractCallerOperation, AbstractOperation, AbstractStartOperation} from "@/model/operation/Operation";
import {Scope} from "@/model/validation/Scope";
import {LoopDependencyError} from "@/model/error/LoopDependencyError";
import {EmptyInputError} from "@/model/error/EmptyInputError";
import {CallableOperationDefinition, CallerOperationDefinition} from "@/model/definition/Definition";
import {OperationCallRequiredError} from "@/model/error/OperationCallRequiredError";
import {RelativeScope} from "@/model/definition/DataType";
import {OutOfScopeAccessError} from "@/model/error/OutOfScopeAccessError";
import {ConstantGetter} from "@/model/operation/library/getter/ConstantGetter";

export class Validation {
    static validate(start: AbstractCallerOperation, operations: AbstractOperation[], constants: ConstantGetter[]) : ValidationErrors {
        let result = new ValidationErrors();
        let validationContext = new ValidationContext(result, Validation.extractRootOperations(operations, constants));

        Validation.validateScope(start, validationContext);
        
        return result;
    }

    private static validateScope(operation: AbstractCallerOperation, validationContext: ValidationContext) {
        try {
            for(let providedOutcomeName in operation.providedOutcomes) {
                let providedOutcome = operation.providedOutcomes[providedOutcomeName];
                let callScope : Scope;
                let actualValidationContext : ValidationContext;
                switch (operation.definition.outcomes[providedOutcomeName].calledIn) {
                    case RelativeScope.CURRENT:
                        callScope = validationContext.findByOperation(operation)!.scope;
                        actualValidationContext = validationContext;
                        break;
                    case RelativeScope.CHILD:
                        callScope = validationContext.findByOperation(operation)!.childScope;
                        actualValidationContext = validationContext.clone();
                        break;
                }
                if(providedOutcome.target.definition.inputNames.length > 0) {
                    let inputScope = Validation.calculateInputScope(providedOutcome.target, actualValidationContext);
                    if(!callScope.equalsOrChildOf(inputScope)) {
                        throw new OutOfScopeAccessError(providedOutcome.target);
                    }
                }
                if(validationContext.findByOperation(providedOutcome.target)) {
                    throw new LoopDependencyError(providedOutcome.target);
                }
                actualValidationContext.addValidOperation(providedOutcome.target, callScope);
                Validation.validateScope(providedOutcome.target as AbstractCallerOperation, actualValidationContext);
            }
        } catch (e) {
            validationContext.addValidationError(operation, e);
        }
    }

    private static extractRootOperations(operations: AbstractOperation[], constants: ConstantGetter[]) : ValidOperation[] {
        let rootScope = new Scope();
        let rootOperations: ValidOperation[] = [];
        for(let operation of operations) {
            if(operation.definition.isRoot) {
                rootOperations.push(new ValidOperation(operation, rootScope));
            }
        }
        for(let constant of constants) {
            rootOperations.push(new ValidOperation(constant, rootScope));
        }
        return rootOperations;
    }

    private static calculateInputScope(operation: AbstractOperation, validationContext: ValidationContext) : Scope {
        return Validation.calculateInputScopeRecursive(operation, validationContext, operation, []);
    }

    private static calculateInputScopeRecursive(operation: AbstractOperation,
                                                validationContext: ValidationContext,
                                                acceptedIncompleteCallable: AbstractOperation,
                                                incompleteOperationsStack: AbstractOperation[]) : Scope {
        let validOperation = validationContext.findByOperation(operation);
        if(validOperation) {
            return validOperation.outputScope;
        }

        if(operation.hasEmptyInputs) {
            throw new EmptyInputError(operation);
        }

        if(operation !== acceptedIncompleteCallable && operation.definition instanceof CallableOperationDefinition) {
            throw new OperationCallRequiredError(operation);
        }

        for(let incompleteOperation of incompleteOperationsStack) {
            if(operation === incompleteOperation) {
                throw new LoopDependencyError(operation);
            }
        }

        incompleteOperationsStack.push(operation);

        let scopes: Scope[] = [];
        for(let providedInputName in operation.providedInputs) {
            let valueSource = operation.providedInputs[providedInputName].source;
            scopes.push(Validation.calculateInputScopeRecursive(
                valueSource, validationContext, acceptedIncompleteCallable, incompleteOperationsStack
            ));
        }

        incompleteOperationsStack.splice(incompleteOperationsStack.indexOf(operation), 1);

        let scope = Scope.findYoungest(scopes);

        if(operation !== acceptedIncompleteCallable) {
            validationContext.addValidOperation(operation, scope);
        }

        return scope;
    }

}

export class ValidOperation {
    private readonly _operation: AbstractOperation;
    private readonly _scope: Scope;
    private readonly _childScope: Scope;
    private readonly _outputScope: Scope;

    constructor(operation: AbstractOperation, scope: Scope) {
        this._operation = operation;
        this._scope = scope;
        this._childScope = new Scope(this._scope);
        this._outputScope = ValidOperation.outputInChildScope(operation) ? this._childScope : this._scope;
    }

    get operation(): AbstractOperation {
        return this._operation;
    }

    get scope(): Scope {
        return this._scope;
    }

    get childScope(): Scope {
        return this._childScope;
    }

    get outputScope(): Scope {
        return this._outputScope;
    }

    private static outputInChildScope(operation: AbstractOperation) : boolean {
        if(operation.definition instanceof CallerOperationDefinition) {
            for(let outcomeName of operation.definition.outcomeNames) {
                if(operation.definition.outcomes[outcomeName].calledIn === RelativeScope.CHILD) {
                    return true;
                }
            }
        }
        return false;
    }

}

export class ValidationContext {
    private readonly _validationErrors: ValidationErrors;
    private readonly _validOperations: ValidOperation[];

    constructor(validationErrors: ValidationErrors, validOperations: ValidOperation[] = []) {
        this._validationErrors = validationErrors;
        this._validOperations = validOperations;
    }

    findByOperation(operation: AbstractOperation) : ValidOperation | null {
        for(let o of this._validOperations) {
            if(o.operation === operation) {
                return o;
            }
        }
        return null;
    }

    addValidOperation(operation: AbstractOperation, scope: Scope) {
        this._validOperations.push(new ValidOperation(operation, scope));
    }

    addValidationError(operation: AbstractOperation, e: Error) {
        this._validationErrors.addValidationError(operation, e);
    }

    clone() : ValidationContext {
        let validOperations : ValidOperation[] = [];
        for(let validOperation of this._validOperations) {
            validOperations.push(validOperation)
        }
        return new ValidationContext(this._validationErrors, validOperations);
    }
}

