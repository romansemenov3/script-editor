import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";

export class LookAction {

    private readonly _matrix: TransformMatrix;
    private readonly _initial: Point;

    constructor(matrix: TransformMatrix, e: MouseEvent) {
        this._matrix = matrix;
        this._initial = this._matrix.inverse({x: e.offsetX, y: e.offsetY});
    }

    public move(e: MouseEvent) {
        let delta = this._matrix.inverse({x: e.offsetX, y: e.offsetY});
        this._matrix.move(delta.x - this._initial.x, delta.y - this._initial.y);
    }

}
