import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";
import {EditableOperation} from "@/model/project/EditableOperation";

export class OperationsDragAction {

    private readonly _initials: Point[] = [];
    private readonly _matrix: TransformMatrix;
    private readonly _editableOperations: EditableOperation[];

    constructor(editableOperations: EditableOperation[], e: MouseEvent, matrix: TransformMatrix) {
        this._matrix = matrix;
        this._editableOperations = editableOperations;

        let start = this._matrix.inverse({x: e.offsetX, y: e.offsetY});
        this._editableOperations.forEach(operation => {
            this._initials.push(
                {
                    x: operation.position.x - start.x,
                    y: operation.position.y - start.y
                }
            )
        });
    }

    public move(e: MouseEvent) {
        let delta = this._matrix.inverse({ x: e.offsetX, y: e.offsetY });
        this._editableOperations.forEach((operation, index) => {
            let initial = this._initials[index];
            let precisePosition : Point = {
                x: delta.x + initial.x + 15 ,
                y: delta.y + initial.y + 15
            };
            operation.position = {
                x: (Math.floor(precisePosition.x/10.0) - 1)*10.0,
                y: (Math.floor(precisePosition.y/10.0) - 1)*10.0
            };
        });
    }

}
