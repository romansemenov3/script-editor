import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";
import {EditableOperation} from "@/model/project/EditableOperation";
import {ValueLinkEvent} from "@/model/ui/event/OperationEvents";
import {ValueLink} from "@/model/project/Link";
import {InputDefinition, OutputDefinition} from "@/model/definition/Definition";
import {UndefinedOutputError} from "@/model/error/UndefinedOutputError";
import {UndefinedInputError} from "@/model/error/UndefinedInputError";
import {TypeMismatchError} from "@/model/error/TypeMismatchError";
import {ColorUtils} from "@/service/ColorUtils";
import {LoopDependencyError} from "@/model/error/LoopDependencyError";

export class ValueLinkAction {

    private _fromOperation: EditableOperation | null;
    private _fromOutput: string | null;

    private _toOperation: EditableOperation | null;
    private _toInput: string | null;

    private _point: Point;
    private readonly _matrix: TransformMatrix;

    constructor(linkEvent: ValueLinkEvent, matrix: TransformMatrix) {
        this._fromOperation = linkEvent.fromEditableOperation;
        this._fromOutput = linkEvent.fromOutput;
        this._toOperation = linkEvent.toEditableOperation;
        this._toInput = linkEvent.toInput;
        this._point = matrix.inverse( { x: linkEvent.e.offsetX, y: linkEvent.e.offsetY } );
        this._matrix = matrix;
    }

    move(e: MouseEvent) {
        let result = this._matrix.inverse({x: e.offsetX, y: e.offsetY });
        result.y -= 6;
        this._point = result;
    }

    end(linkEvent: ValueLinkEvent) : ValueLink {
        let fromOperation = this.fromOperation || linkEvent.fromEditableOperation;
        let fromOutput = this.fromOutput || linkEvent.fromOutput;
        let toOperation = this.toOperation || linkEvent.toEditableOperation;
        let toInput = this.toInput || linkEvent.toInput;

        let output: OutputDefinition = fromOperation.definition.outputs[fromOutput];
        if(!output) {
            throw new UndefinedOutputError(fromOperation.operation, fromOutput);
        }
        let input: InputDefinition = toOperation.definition.inputs[toInput];
        if(!input) {
            throw new UndefinedInputError(toOperation.operation, toInput);
        }
        if(!input.accepts(output)) {
            throw new TypeMismatchError(toOperation.operation, toInput, output);
        }
        if(fromOperation === toOperation) {
            throw new LoopDependencyError(toOperation.operation);
        }

        return new ValueLink(fromOperation, fromOutput, toOperation, toInput);
    }

    get fromOperation(): EditableOperation | null {
        return this._fromOperation;
    }

    get fromOutput(): string | null {
        return this._fromOutput;
    }

    get fromOutputPosition() : Point {
        if(this.fromOperation && this.fromOutput) {
            return {
                x: this.fromOperation.position.x + this.fromOperation.element.width,
                y: this.fromOperation.position.y + this.fromOperation.element.body.outputsTop[this.fromOutput]
            }
        }
        return this.point;
    }

    get toOperation(): EditableOperation | null {
        return this._toOperation;
    }

    get toInput(): string | null {
        return this._toInput;
    }

    get toInputPosition() : Point {
        if(this.toOperation && this.toInput) {
            return {
                x: this.toOperation.position.x,
                y: this.toOperation.position.y + this.toOperation.element.body.inputsTop[this.toInput]
            }
        }
        return this.point;
    }

    get point(): Point {
        return this._point;
    }

    get path() : string {
        let from : Point = this.fromOutputPosition;
        let to : Point = this.toInputPosition;
        let delta : Point = { x: to.x - from.x, y: to.y - from.y };
        let distance = Math.sqrt(delta.x*delta.x + delta.y*delta.y);
        let curvature = distance/2;

        return "M" + from.x + " " + from.y + " C " + (from.x + curvature) + " " + from.y + ", " +
            (to.x - curvature) + " " + to.y + ", " + to.x + " " + to.y;
    }

    get color() : string {
        if(this.fromOperation && this.fromOutput) {
            return ColorUtils.getTypeColor(this.fromOperation.definition.outputs[this.fromOutput].type);
        }
        if(this.toOperation && this.toInput) {
            return ColorUtils.getTypeColor(this.toOperation.definition.inputs[this.toInput].type);
        }
        return "black"
    }

}
