import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";
import {EditableOperation} from "@/model/project/EditableOperation";
import {CallLinkEvent} from "@/model/ui/event/OperationEvents";
import {CallLink} from "@/model/project/Link";
import {LoopDependencyError} from "@/model/error/LoopDependencyError";

export class CallLinkAction {

    private _fromOperation: EditableOperation | null;
    private _fromOutcome: string | null;

    private _toOperation: EditableOperation | null;

    private _point: Point;
    private readonly _matrix: TransformMatrix;

    constructor(linkEvent: CallLinkEvent, matrix: TransformMatrix) {
        this._fromOperation = linkEvent.fromEditableOperation;
        this._fromOutcome = linkEvent.fromOutcome;
        this._toOperation = linkEvent.toEditableOperation;
        this._point = matrix.inverse( { x: linkEvent.e.offsetX, y: linkEvent.e.offsetY } );
        this._matrix = matrix;
    }

    move(e: MouseEvent) {
        let result = this._matrix.inverse({x: e.offsetX, y: e.offsetY });
        result.y -= 7;
        this._point = result;
    }

    end(linkEvent: CallLinkEvent) : CallLink {
        let fromOperation = this.fromOperation || linkEvent.fromEditableOperation;
        let fromOutcome = this.fromOutcome || linkEvent.fromOutcome;
        let toOperation = this.toOperation || linkEvent.toEditableOperation;

        if(fromOperation === toOperation) {
            throw new LoopDependencyError(toOperation.operation);
        }

        return new CallLink(fromOperation, fromOutcome, toOperation);
    }

    get fromOperation(): EditableOperation | null {
        return this._fromOperation;
    }

    get fromOutcome(): string | null {
        return this._fromOutcome;
    }

    get fromOutcomePosition() : Point {
        if(this.fromOperation && this.fromOutcome) {
            return {
                x: this.fromOperation.position.x + this.fromOperation.element.width,
                y: this.fromOperation.position.y + this.fromOperation.element.body.outcomesTop[this.fromOutcome]
            }
        }
        return this.point;
    }

    get toOperation(): EditableOperation | null {
        return this._toOperation;
    }

    get toCallPosition() : Point {
        if(this.toOperation) {
            return {
                x: this.toOperation.position.x,
                y: this.toOperation.position.y
            }
        }
        return this.point;
    }

    get point(): Point {
        return this._point;
    }

    get path() : string {
        let from : Point = this.fromOutcomePosition;
        let to : Point = this.toCallPosition;
        let delta : Point = { x: to.x - from.x, y: to.y - from.y };
        let distance = Math.sqrt(delta.x*delta.x + delta.y*delta.y);
        let curvature = distance/2;

        return "M" + from.x + " " + from.y + " C " + (from.x + curvature) + " " + from.y + ", " +
            (to.x - curvature) + " " + to.y + ", " + to.x + " " + to.y;
    }

}
