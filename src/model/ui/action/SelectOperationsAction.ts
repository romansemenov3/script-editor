import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";
import {EditableOperation} from "@/model/project/EditableOperation";

export class SelectOperationsAction {

    private readonly _initial: Point;
    private readonly _matrix: TransformMatrix;
    private _opposite: Point;

    constructor(matrix: TransformMatrix, e: MouseEvent) {
        this._matrix = matrix;
        this._initial = {x: e.offsetX, y: e.offsetY};
        this._opposite = this._initial;
    }

    get top() : number {
        return Math.min(this._initial.y, this._opposite.y);
    }

    get bottom() : number {
        return Math.max(this._initial.y, this._opposite.y);
    }

    get left() : number {
        return Math.min(this._initial.x, this._opposite.x);
    }

    get right() : number {
        return Math.max(this._initial.x, this._opposite.x);
    }

    get height() : number {
        return Math.abs(this._initial.y - this._opposite.y);
    }

    get width() : number {
        return Math.abs(this._initial.x - this._opposite.x);
    }

    public move(e: MouseEvent) {
        this._opposite = {x: e.offsetX, y: e.offsetY};
    }

    public getIntersected(operations: EditableOperation[]) : EditableOperation[] {
        let result : EditableOperation[] = [];
        for(let operation of operations) {
            if(this.isOperationInsideSelection(operation)) {
                result.push(operation);
            }
        }
        return result;
    }

    private isOperationInsideSelection(operation: EditableOperation) : boolean {
        let operationTopLeft = this._matrix.transform({
            x: operation.position.x - 15,
            y: operation.position.y - operation.element.header.height - 15
        });
        let operationBottomRight = this._matrix.transform({
            x: operation.position.x + operation.element.width + 15,
            y: operation.position.y + operation.element.height - operation.element.header.height + 15
        });

        if(this.left > operationBottomRight.x) {
            return false;
        }
        if(this.right < operationTopLeft.x) {
            return false;
        }

        if(this.top > operationBottomRight.y) {
            return false;
        }
        if(this.bottom < operationTopLeft.y) {
            return false;
        }

        return true;
    }

}
