import {Point, TransformMatrix} from "@/model/ui/TransformMatrix";

export class ScaleAction {

    private readonly _scaleIn: number;
    private readonly _scaleOut: number;
    private readonly _minScale: number;
    private readonly _maxScale: number;
    private readonly _matrix: TransformMatrix;
    private _value: number = 1;

    constructor(scale: number, minScale: number, maxScale: number, matrix: TransformMatrix) {
        this._scaleIn = scale;
        this._scaleOut = 1/this._scaleIn;
        this._minScale = minScale;
        this._maxScale = maxScale;
        this._matrix = matrix;
    }

    get value(): number {
        return this._value;
    }

    public scale(e: WheelEvent) {
        if(e.deltaY > 0) {
            if(this._value > this._minScale) {
                this._matrix.scaleAt(this._scaleOut, this.shiftAndInverse(e));
                this._value *= this._scaleOut;
            }
        } else {
            if(this._value < this._maxScale) {
                this._matrix.scaleAt(this._scaleIn, this.shiftAndInverse(e));
                this._value *= this._scaleIn;
            }
        }
    }

    private shiftAndInverse(e: WheelEvent) : Point {
        return this._matrix.inverse(ScaleAction.shift(e));
    }

    private static shift(e: WheelEvent) : Point {
        let element = e.currentTarget as Element;
        let rect = element.getBoundingClientRect();
        return {
            x: e.x - rect.x,
            y: e.y - rect.y,
        };
    }

}
