import {DataCollection, DataType} from "@/model/definition/DataType";
import {InputDefinition, OutputDefinition} from "@/model/definition/Definition";

export class ValueHighlight {

    private _highlightInput: boolean = false;
    private _highlightOutput: boolean = false;
    private _type: DataType | null = null;
    private _collection: DataCollection | null = null;

    highlightInputs(type: DataType, collection: DataCollection) {
        this._type = type;
        this._collection = collection;
        this._highlightInput = true;
    }

    highlightOutputs(type: DataType, collection: DataCollection) {
        this._type = type;
        this._collection = collection;
        this._highlightOutput = true;
    }

    hideAll() {
        this._highlightOutput = true;
        this._highlightInput = true;
    }

    resetHighlight() {
        this._type = null;
        this._collection = null;
        this._highlightInput = false;
        this._highlightOutput = false;
    }

    isInputHidden(input: InputDefinition) : boolean {
        return this._highlightOutput ||
            (this._highlightInput && !(this._type === input.type && this._collection === input.collection));
    }

    isOutputHidden(output: OutputDefinition) : boolean {
        return this._highlightInput ||
            (this._highlightOutput && !(this._type === output.type && this._collection === output.collection));
    }

}

export class CallHighlight {

    private _highlightCall: boolean = false;
    private _highlightOutcomes: boolean = false;

    highlightCalls() {
        this._highlightCall = true;
    }

    highlightOutcomes() {
        this._highlightOutcomes = true;
    }

    hideAll() {
        this._highlightCall = true;
        this._highlightOutcomes = true;
    }

    resetHighlight() {
        this._highlightCall = false;
        this._highlightOutcomes = false;
    }

    isCallHidden() : boolean {
        return this._highlightOutcomes;
    }

    isOutcomeHidden() : boolean {
        return this._highlightCall;
    }

}

export class InputElementsHighlight {

    private _disableInputElements: boolean = false;

    disableAll() {
        this._disableInputElements = true;
    }

    enableAll() {
        this._disableInputElements = false;
    }

    get disableInputElements(): boolean {
        return this._disableInputElements;
    }
}
