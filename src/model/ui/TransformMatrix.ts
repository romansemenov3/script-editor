export interface Point {
    x: number;
    y: number;
}

export class TransformMatrix {

    private _a: number = 1;
    private _b: number = 0;
    private _c: number = 0;
    private _d: number = 1;
    private _e: number = 0;
    private _f: number = 0;

    private _ia: number = this._a;
    private _ib: number = this._b;
    private _ic: number = this._c;
    private _id: number = this._d;
    private _ie: number = this._e;
    private _if: number = this._f;

    private updateInverseMatrix() {
        let adbc = this._a*this._d - this._b*this._c;
        let nadbc = -adbc;

        this._ia = this._d/adbc;
        this._ib = this._b/nadbc;
        this._ic = this._c/nadbc;
        this._id = this._a/adbc;
        this._ie = (this._d*this._e - this._c*this._f)/nadbc;
        this._if = (this._b*this._e - this._a*this._f)/adbc;
    }

    public move(dx: number, dy: number) {
        this._e += this._a*dx + this._c*dy;
        this._f += this._b*dx + this._d*dy;

        this.updateInverseMatrix();
    }

    public scale(ds: number) {
        this._a *= ds;
        this._b *= ds;
        this._c *= ds;
        this._d *= ds;

        this.updateInverseMatrix();
    }

    public scaleAt(ds: number, p: Point) {
        this.move((1-ds)*p.x, (1-ds)*p.y);
        this.scale(ds);
    }

    public rotate(da: number) {
        let sin = Math.sin(da);
        let cos = Math.cos(da);

        let a = this._a*cos - this._c*sin;
        let b = this._b*cos - this._d*sin;
        let c = this._a*sin + this._c*cos;
        let d = this._b*sin + this._d*cos;

        this._a = a;
        this._b = b;
        this._c = c;
        this._d = d;

        this.updateInverseMatrix();
    }

    public apply(t: TransformMatrix) {
        let a = this._a*t._a + this._c*t._b;
        let b = this._b*t._a - this._d*t._b;
        let c = this._a*t._c + this._c*t._d;
        let d = this._b*t._c - this._d*t._d;
        let e = this._e + this._a*t._e + this._c*t._f;
        let f = this._f + this._b*t._e + this._d*t._f;

        this._a = a;
        this._b = b;
        this._c = c;
        this._d = d;
        this._e = e;
        this._f = f;

        this.updateInverseMatrix();
    }

    public transform(p: Point) : Point {
        return {
            x: this._a*p.x + this._c*p.y + this._e,
            y: this._b*p.x + this._d*p.y + this._f
        }
    }

    public inverse(p: Point) : Point {
        return {
            x: this._ia*p.x + this._ic*p.y + this._ie,
            y: this._ib*p.x + this._id*p.y + this._if
        }
    }

    get matrix() : string {
        return "matrix(" +
            this._a + "," +
            this._b + "," +
            this._c + "," +
            this._d + "," +
            this._e + "," +
            this._f + ")";
    }

}
