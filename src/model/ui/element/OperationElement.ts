import {
    DefaultOperationHeaderElement,
    EmptyHeaderElement,
    OperationHeaderElement
} from "@/model/ui/element/OperationHeaderElement";
import {
    DefaultOperationBodyElement,
    OperationBodyElement,
    PassBodyElement
} from "@/model/ui/element/OperationBodyElement";
import {OperationDefinition} from "@/model/definition/Definition";
import {AbstractOperation} from "@/model/operation/Operation";

const HEADER_HEIGHT = 30;
const HEADER_FONT = "20px Arial";

export class OperationElement {

    private readonly _header: OperationHeaderElement;
    private readonly _body: OperationBodyElement;

    constructor(header: OperationHeaderElement, body: OperationBodyElement) {
        this._header = header;
        this._body = body;
    }

    get header(): OperationHeaderElement {
        return this._header;
    }

    get body(): OperationBodyElement {
        return this._body;
    }

    get width() : number {
        return Math.max(this._header.width, this._body.width);
    }

    get height() : number {
        return this._header.height + this._body.height;
    }

    public static fromAnyOperation(operation: AbstractOperation) : OperationElement {
        return new OperationElement(
            new DefaultOperationHeaderElement(HEADER_HEIGHT, HEADER_FONT, operation.definition),
            new DefaultOperationBodyElement(operation.definition)
        );
    }

    public static fromPass(operation: AbstractOperation) : OperationElement {
        return new OperationElement(
            new EmptyHeaderElement(),
            new PassBodyElement(operation)
        );
    }

}
