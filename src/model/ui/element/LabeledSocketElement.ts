import {LabelElement} from "@/model/ui/element/LabelElement";

export class LabeledSocketElement {

    private readonly _label: LabelElement;
    private readonly _labelIndent: number;

    constructor(labelFont: string, labelText: string, labelIndent: number) {
        this._label = new LabelElement(labelFont, labelText);
        this._labelIndent = labelIndent;
    }

    get label(): LabelElement {
        return this._label;
    }

    get labelIndent(): number {
        return this._labelIndent;
    }

    get width() : number {
        return this._label.width + this._labelIndent;
    }

}
