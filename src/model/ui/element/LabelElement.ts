import {StringUtils} from "@/service/StringUtils";

export class LabelElement {

    private readonly _font: string;
    protected _text: string;
    protected _width: number;

    constructor(font: string, text: string) {
        this._font = font;
        this._text = text;
        this._width = StringUtils.getTextWidth(this._text, this._font);
    }

    get font(): string {
        return this._font;
    }

    get width(): number {
        return this._width;
    }

    get text(): string {
        return this._text;
    }

}
