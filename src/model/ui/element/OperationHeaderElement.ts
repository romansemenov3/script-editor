import {LabelElement} from "@/model/ui/element/LabelElement";
import {OperationDefinition} from "@/model/definition/Definition";

export interface OperationHeaderElement {
    readonly height: number;
    readonly width: number;
}

export class DefaultOperationHeaderElement implements OperationHeaderElement {
    private readonly _height: number;
    private readonly _label: LabelElement;

    constructor(height: number, font: string, definition: OperationDefinition) {
        this._height = height;
        this._label = new LabelElement(font, definition.name);
    }

    get height(): number {
        return this._height;
    }

    get width() : number {
        return this._label.width;
    }
}

export class EmptyHeaderElement implements OperationHeaderElement {

    get height(): number {
        return 0;
    }

    get width() : number {
        return 0;
    }

}
