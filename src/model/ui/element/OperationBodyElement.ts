import {LabeledSocketElement} from "@/model/ui/element/LabeledSocketElement";
import {LabeledInputTextSocketElement} from "@/model/ui/element/LabeledInputTextSocketElement";
import {
    CallableOperationDefinition,
    CallerOperationDefinition,
    InputDefinition,
    OperationDefinition
} from "@/model/definition/Definition";
import {DataCollection, DataType} from "@/model/definition/DataType";
import {LabeledCheckboxSocketElement} from "@/model/ui/element/LabeledCheckboxSocketElement";
import {ColorUtils} from "@/service/ColorUtils";
import {AbstractOperation} from "@/model/operation/Operation";
import {CallablePass} from "@/model/operation/library/pass/CallablePass";
import {ValuePass} from "@/model/operation/library/pass/ValuePass";

const LABEL_FONT = "14px Arial";
const INPUT_FONT = "13px Arial";

const LABEL_INDENT = 10;
const ITEM_HEIGHT = 30;
const CHECKBOX_WIDTH = 20;
const INPUTS_OUTPUTS_GAP = 50;
const INPUT_MIN_WIDTH = 20;

export interface OperationBodyElement {
    readonly inputs: Record<string, LabeledSocketElement>;
    readonly outcomes: Record<string, LabeledSocketElement>;
    readonly outputs: Record<string, LabeledSocketElement>;

    readonly inputsTop: Record<string, number>;
    readonly outcomesTop: Record<string, number>;
    readonly outputsTop: Record<string, number>;

    readonly width : number;
    readonly height : number;

    readonly inputsColor: Record<string, string>;
    readonly outputsColor: Record<string, string>;
}

export class DefaultOperationBodyElement implements OperationBodyElement {

    private readonly _inputs: Record<string, LabeledSocketElement> = {};
    private readonly _outcomes: Record<string, LabeledSocketElement> = {};
    private readonly _outputs: Record<string, LabeledSocketElement> = {};

    private readonly _inputsTop: Record<string, number> = {};
    private readonly _outcomesTop: Record<string, number> = {};
    private readonly _outputsTop: Record<string, number> = {};

    private readonly _inputsColor: Record<string, string> = {};
    private readonly _outputsColor: Record<string, string> = {};

    private readonly _height: number;

    constructor(definition: OperationDefinition) {
        let inputsHeightShift = 0;
        let callsAndInputsCount = definition.inputNames.length;
        if(definition instanceof CallableOperationDefinition) {
            inputsHeightShift = ITEM_HEIGHT;
            callsAndInputsCount += 1;
        }

        for(let inputIndex in definition.inputNames) {
            let inputName = definition.inputNames[inputIndex];
            let inputDefinition = definition.inputs[inputName];

            this._inputs[inputName] = DefaultOperationBodyElement.buildInputByDefinition(inputName, inputDefinition);
            this._inputsTop[inputName] = inputsHeightShift + ITEM_HEIGHT*Number.parseInt(inputIndex);
            this._inputsColor[inputName] = ColorUtils.getTypeColor(inputDefinition.type);
        }

        let outputsHeightShift = 0;
        let outcomesAndOutputsCount = definition.outputNames.length;
        if(definition instanceof CallerOperationDefinition) {
            outputsHeightShift = ITEM_HEIGHT*definition.outcomeNames.length;
            outcomesAndOutputsCount += definition.outcomeNames.length;

            for(let outcomeIndex in definition.outcomeNames) {
                let outcomeName = definition.outcomeNames[outcomeIndex];

                this._outcomes[outcomeName] = new LabeledSocketElement(LABEL_FONT, outcomeName, LABEL_INDENT);
                this._outcomesTop[outcomeName] = ITEM_HEIGHT*Number.parseInt(outcomeIndex)
            }
        }

        for(let outputIndex in definition.outputNames) {
            let outputName = definition.outputNames[outputIndex];
            let outputDefinition = definition.outputs[outputName];

            this._outputs[outputName] = new LabeledSocketElement(LABEL_FONT, outputName, LABEL_INDENT);
            this._outputsTop[outputName] = outputsHeightShift + ITEM_HEIGHT*Number.parseInt(outputIndex);
            this._outputsColor[outputName] = ColorUtils.getTypeColor(outputDefinition.type);
        }

        this._height = ITEM_HEIGHT*Math.max(0, Math.max(callsAndInputsCount, outcomesAndOutputsCount) - 1);
    }

    get inputs(): Record<string, LabeledSocketElement> {
        return this._inputs;
    }

    get outcomes(): Record<string, LabeledSocketElement> {
        return this._outcomes;
    }

    get outputs(): Record<string, LabeledSocketElement> {
        return this._outputs;
    }

    get inputsTop(): Record<string, number> {
        return this._inputsTop;
    }

    get outcomesTop(): Record<string, number> {
        return this._outcomesTop;
    }

    get outputsTop(): Record<string, number> {
        return this._outputsTop;
    }

    get width() : number {
        let widestInput = 0;
        for(let inputName in this._inputs) {
            if(widestInput < this._inputs[inputName].width) {
                widestInput = this._inputs[inputName].width;
            }
        }

        let widestOutput = 0;
        for(let outcomeName in this._outcomes) {
            if(widestOutput < this._outcomes[outcomeName].width) {
                widestOutput = this._outcomes[outcomeName].width;
            }
        }
        for(let outputName in this._outputs) {
            if(widestOutput < this._outputs[outputName].width) {
                widestOutput = this._outputs[outputName].width;
            }
        }

        return widestInput + INPUTS_OUTPUTS_GAP + widestOutput;
    }

    get height() : number {
        return this._height;
    }

    get inputsColor(): Record<string, string> {
        return this._inputsColor;
    }

    get outputsColor(): Record<string, string> {
        return this._outputsColor;
    }

    private static buildInputByDefinition(inputName: string, inputDefinition: InputDefinition) : LabeledSocketElement {
        if(inputDefinition.collection === DataCollection.SINGLE) {
            switch (inputDefinition.type) {
                case DataType.BOOLEAN:
                    return new LabeledCheckboxSocketElement(LABEL_FONT, inputName, LABEL_INDENT, CHECKBOX_WIDTH);
                case DataType.NUMBER:
                    return new LabeledInputTextSocketElement(LABEL_FONT, inputName, LABEL_INDENT, INPUT_FONT, "", INPUT_MIN_WIDTH);
                case DataType.STRING:
                    return new LabeledInputTextSocketElement(LABEL_FONT, inputName, LABEL_INDENT, INPUT_FONT, "", INPUT_MIN_WIDTH);
            }
        }
        return new LabeledSocketElement(LABEL_FONT, inputName, LABEL_INDENT);
    }

}

export class PassBodyElement implements OperationBodyElement {

    private readonly _inputs: Record<string, LabeledSocketElement> = {};
    private readonly _outcomes: Record<string, LabeledSocketElement> = {};
    private readonly _outputs: Record<string, LabeledSocketElement> = {};

    private readonly _inputsTop: Record<string, number> = {};
    private readonly _outcomesTop: Record<string, number> = {};
    private readonly _outputsTop: Record<string, number> = {};

    private readonly _inputsColor: Record<string, string> = {};
    private readonly _outputsColor: Record<string, string> = {};

    constructor(passOperation: AbstractOperation) {
        if(passOperation instanceof CallablePass) {
            this._outcomes["After"] = new LabeledSocketElement(LABEL_FONT, "After", 0);
            this._outcomesTop["After"] = 0;
        } else if(passOperation instanceof ValuePass) {
            this._inputs["Value"] = new LabeledSocketElement(LABEL_FONT, "Value", 0);
            this._outputs["Value"] = new LabeledSocketElement(LABEL_FONT, "Value", 0);

            this._inputsTop["Value"] = 0;
            this._outputsTop["Value"] = 0;

            this._inputsColor["Value"] = ColorUtils.getTypeColor(passOperation.definition.inputs["Value"].type);
            this._outputsColor["Value"] = ColorUtils.getTypeColor(passOperation.definition.outputs["Value"].type);
        } else {
            throw new Error("Could not create PassBodyElement for unsupported type")
        }
    }

    get inputs(): Record<string, LabeledSocketElement> {
        return this._inputs;
    }

    get outcomes(): Record<string, LabeledSocketElement> {
        return this._outcomes;
    }

    get outputs(): Record<string, LabeledSocketElement> {
        return this._outputs;
    }

    get inputsTop(): Record<string, number> {
        return this._inputsTop;
    }

    get outcomesTop(): Record<string, number> {
        return this._outcomesTop;
    }

    get outputsTop(): Record<string, number> {
        return this._outputsTop;
    }

    get inputsColor(): Record<string, string> {
        return this._inputsColor;
    }

    get outputsColor(): Record<string, string> {
        return this._outputsColor;
    }

    get height(): number {
        return 0;
    }

    get width() : number {
        return 10;
    }

}
