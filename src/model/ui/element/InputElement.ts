import {LabelElement} from "@/model/ui/element/LabelElement";
import {StringUtils} from "@/service/StringUtils";

export class InputElement extends LabelElement {

    private readonly _minWidth: number;

    constructor(font: string, text: string, minWidth: number) {
        super(font, text);
        this._minWidth = minWidth;
        this._width = Math.max(this._minWidth, StringUtils.getTextWidth(this._text, this.font));
    }

    get minWidth(): number {
        return this._minWidth;
    }

    get text() : string {
        return super.text;
    }

    set text(value: string) {
        this._text = value;
        this._width = Math.max(this._minWidth, StringUtils.getTextWidth(this._text, this.font));
    }

}
