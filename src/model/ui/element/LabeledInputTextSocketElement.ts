import {InputElement} from "@/model/ui/element/InputElement";
import {LabeledSocketElement} from "@/model/ui/element/LabeledSocketElement";

export class LabeledInputTextSocketElement extends LabeledSocketElement {

    private readonly _input: InputElement;

    constructor(labelFont: string, labelText: string, labelIndent: number, inputFont: string, inputText: string, inputMinWidth: number) {
        super(labelFont, labelText, labelIndent);
        this._input = new InputElement(inputFont, inputText, inputMinWidth);
    }

    get input() : InputElement {
        return this._input;
    }

    get width() : number {
        return super.width + super.labelIndent + this._input.width;
    }

}
