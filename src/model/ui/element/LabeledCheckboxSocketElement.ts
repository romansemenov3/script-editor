import {LabeledSocketElement} from "@/model/ui/element/LabeledSocketElement";

export class LabeledCheckboxSocketElement extends LabeledSocketElement {

    private readonly _checkboxWidth: number;
    private _checked: boolean = false;

    constructor(labelFont: string, labelText: string, labelIndent: number, checkboxWidth: number) {
        super(labelFont, labelText, labelIndent);
        this._checkboxWidth = checkboxWidth;
    }

    get width() : number {
        return super.width + super.labelIndent + this._checkboxWidth;
    }

    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        this._checked = value;
    }
}
