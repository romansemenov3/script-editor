import {EditableOperation} from "@/model/project/EditableOperation";
import {CallLink, ValueLink} from "@/model/project/Link";

export interface StartOperationDragEvent {
    editableOperation: EditableOperation,
    e: MouseEvent
}

export interface ValueLinkEvent {
    fromEditableOperation: EditableOperation,
    fromOutput: string,

    toEditableOperation: EditableOperation,
    toInput: string,

    e: MouseEvent
}

export interface CallLinkEvent {
    fromEditableOperation: EditableOperation,
    fromOutcome: string,

    toEditableOperation: EditableOperation,

    e: MouseEvent
}

export interface SplitValueLinkEvent {
    valueLink: ValueLink,

    e: MouseEvent
}

export interface SplitCallLinkEvent {
    callLink: CallLink,

    e: MouseEvent
}
