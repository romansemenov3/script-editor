const CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const CHARACTERS_LENGTH = CHARACTERS.length;

export abstract class StringUtils {

    private static getRandomCharacter(): string {
        return CHARACTERS.charAt(Math.floor(Math.random() * CHARACTERS_LENGTH));
    }

    public static generateString(length: number) : string {
        let result = "";
        for(let i = 0; i < length; i++) {
            result += StringUtils.getRandomCharacter();
        }
        return result;
    }

    public static generateAlias() : string {
        return StringUtils.generateString(4);
    }

    public static indent(length: number) : string {
        let result = "";
        for(let i = 0; i < length; i++) {
            result += "\t";
        }
        return result;
    }

    private static buildContext() : CanvasRenderingContext2D {
        let canvas = document.getElementById("context-holder") as HTMLCanvasElement;
        if(!canvas) {
            throw new Error("Could not get context-holder");
        }
        let context = canvas.getContext("2d");
        if(!context) {
            throw new Error("Could not get context");
        }
        return context;
    }

    public static getTextWidth(text: string, font: string) : number {
        if(text === "") {
            return 0;
        }

        let context = StringUtils.buildContext();
        context.font = font;
        return context.measureText(text).width;
    }

}
