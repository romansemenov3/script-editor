import {Point} from "@/model/ui/TransformMatrix";

export abstract class PointsUtils {

    static translatePosition(p: Point, dx: number, dy: number) : Point {
        return {
            x: p.x + dx,
            y: p.y + dy
        }
    }

    static translatePositions(points: Record<string, Point>, dx: number, dy: number) : Record<string, Point> {
        let result : Record<string, Point> = {};
        for(let key in points) {
            result[key] = PointsUtils.translatePosition(points[key], dx, dy);
        }
        return result;
    }

}
