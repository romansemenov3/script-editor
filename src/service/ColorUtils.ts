import {DataType} from "@/model/definition/DataType";

export abstract class ColorUtils {

    static getTypeColor(type: DataType) : string {
        switch (type) {
            case DataType.BOOLEAN:
                return "maroon";
            case DataType.NUMBER:
                return "darkgreen";
            case DataType.STRING:
                return "blue";
        }
        return "black";
    }

}
